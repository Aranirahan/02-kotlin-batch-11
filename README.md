Kotlin

Variable

var -> bisa dirubah
val -> tidak bisa dirubah

keyword(var or val) namaVariable = isiVariable

Kotlin type data :
* Double -> menampung angka koma-koma yang panjang
* Float -> menampung angka koma-koma tidak panjang
* Long -> Angka yang sangat panjang
* Int -> Angka
Short, Byte -> short 16bit, byte 8 bit, angka
* Boolean -> true, false 
* String -> "Kumpulan kata"
* Char -> 'karakter'

(*) type data yang paling sering dipakai

Boolean
operasi logika ( && AND, || OR, >=, <=, !=, ==,)

Function
// fun functionName(): ReturnType {...}

// parameter function
// fun namaFungsi(namaVariable: Typedata): ReturnType {...}

fun namaFungsi(a: Int): Int {
    return a
}

